package hwo.util;

public class HWOMath 
{

	static public float round2(double val)
	{
		val = val * 100;
		val = Math.round(val);
		return (float)(val) / 100.0f;
	}
	
	static public float round3(double val)
	{
		val = val * 1000;
		val = Math.round(val);
		return (float)(val) / 1000.0f;
	}
}
