package hwo.race;

public class TurboInfo {
	
    private double m_factor;
    private int m_duractionTicks;
    
	public TurboInfo(int turboDurationTicks, double turboFactor) 
	{
		m_duractionTicks = turboDurationTicks;
		m_factor = turboFactor;
	}

	public boolean isAvailable()
	{
		return m_factor > 0.0;
	}
	
	public double getFactor()
	{
		return m_factor;
	}
	
	public int getDuration()
	{
		return m_duractionTicks;
	}

}
