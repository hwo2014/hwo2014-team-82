package hwo.race;

import hwo.message.GameInit.Data.RaceSession;
import hwo.message.Ident;

public class Race 
{
	
	// private double pos[] = new double[2];
	private Track m_track;
	
    private Car m_cars[];
    private int m_tick = 0;
    
    private Car m_myCar;
    private RaceInfo m_raceInfo = new RaceInfo();
    private TurboInfo m_turbo;
    private RaceSession m_session;
    
    private java.util.List<RaceListener> m_listeners = new java.util.ArrayList<RaceListener>();
    
    public Race()
    {
    	
    }
    
    public RaceInfo getInfo()
    {
    	return m_raceInfo;
    }
    
	public void setTrack(Track track)
	{
		m_track = track;
	}

    
    public void addListener(RaceListener listener)
    {
    	m_listeners.add(listener);
    }
    
    public void removeListener(RaceListener listener)
    {
    	m_listeners.remove(listener);
    }
    
    public void start()
    {
    	m_turbo = null;
    	for (RaceListener element : m_listeners)
    	{
    		element.onRaceStarted();
    	}
    }

    public void stop()
    {
    	for (RaceListener element : m_listeners)
    	{
    		element.onRaceEnded();
    	}
    }
    
    public int getTick()
    {
    	return m_tick;
    }
    
    public void setTick(int tick)
    {
    	m_tick = tick;
    }
    
    public Car findCar(Ident id)
    {
    	for (int i = 0; i < m_cars.length; i++)
    	{
    		if (m_cars[i].hasMatchingId(id))
    		{
    			return m_cars[i];
    		}
    	}
    	return null;
    }
    
    public void initCars(int numCars)
    {
    	m_cars = new Car[numCars];
    }

    public boolean isMyCar(Car car)
    {
    	return car == m_myCar;
    }
    
	public void setCar(int i, Car car) 
	{
		m_cars[i] = car;		
	}
	
	public void setMyCar(Car car)
	{
		m_myCar = car;
	}
	
	public void setThrottle(int input) 
	{
		m_myCar.setThrottle(input);
	}

	public void switchRight() 
	{
		m_myCar.setDir(1);			
	}

	public void switchLeft() 
	{
		m_myCar.setDir(-1);	
	}
	

	public void useTurbo() 
	{
		m_myCar.useTurbo(m_turbo);		
	}

	public double getAngle() 
	{
		return m_myCar.getAngle();
	}

	public void update() 
	{
		if (m_cars != null)
		{
			for (int i = 0; i < m_cars.length; i++)
			{
				m_cars[i].update();
			}
		}
	}

	public Car[] getCars() 
	{
		return m_cars;
	}

	public void setSession(RaceSession raceSession) 
	{
		m_session = raceSession;		
	}

	public Track getTrack() 
	{
		return m_track;
	}

	public CarState getCarState() 
	{
		return m_myCar.getCarState();
	}

	public void setTurboInfo(TurboInfo turbo)
	{
		m_turbo = turbo;
	}

	public TurboInfo getTurboInfo()
	{
		return m_turbo;
	}


}
