package hwo.race;

import hwo.analysis.Params;

public class RaceInfo 
{
	public final static int CRASH_TICKS = 4*100-1;
	public final static double CRASH_LIMIT = 60.0;
	public final static double SWITCH_LEN = 2.060; // 2749929336284;	
	
	private final Params m_params = new Params();

	public RaceInfo()
	{
		
	}
	
	public Params getParams()
	{
		return m_params;
	}	
	
}
