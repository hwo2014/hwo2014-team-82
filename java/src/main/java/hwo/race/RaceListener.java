package hwo.race;

public interface RaceListener {
	
	public void onRaceStarted();
	public void onRaceEnded();

}
