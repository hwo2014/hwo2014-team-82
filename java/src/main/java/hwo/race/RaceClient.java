package hwo.race;

import hwo.ai.DriverBot;
import hwo.base.Bot;
import hwo.base.NetClient;
import hwo.base.Version;
import hwo.message.MsgProcessor;

import java.io.BufferedReader;
import java.io.IOException;

public class RaceClient extends NetClient 
{
	private int m_step;
	private Bot m_bot;
	
	private int m_localId;
    private String m_botName;
    private String m_botKey;
    private String m_track = null;
    private int m_numSlots = 1;
    private String m_password = "";

	
    public RaceClient(int id, String host, int port, String botName, String botKey)  
    {
    	super(host, port);
    	m_localId = id;
    	m_botName = botName;
    	m_botKey = botKey;
    	// Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
    }
   
    public RaceClient(int id, String host, int port, String botName, String botKey,
			String track, int numSlots, String password) 
    {
    	super(host, port);
    	m_localId = id;
    	m_botName = botName;
    	m_botKey = botKey;
    	m_track = track;
    	m_numSlots = numSlots;
    	m_password = password;
	}

	@SuppressWarnings("unused")
    protected void run(BufferedReader reader) throws IOException
    {
    	if (Version.MODE != Version.MODE_SAMPLER)
		{
    		System.out.println("Begin");
		}

    	System.out.println("[ID="+m_localId+"]Joining "+m_botName+ " with key "+m_botKey);
    	if (m_track == null)
    	{
    		send(new hwo.message.JoinMsg(m_botName, m_botKey));
    	}
    	else
    	{
    		System.out.println("[ID="+m_localId+"]track: "+m_track+ " numCars:"+m_numSlots+ " Password:"+m_password);
    		sendJoinRace(m_botName, m_botKey, m_track, m_numSlots, m_password);
    		
    	}   	
    	
    	m_bot = new DriverBot(m_race);    	
    	m_bot.start();
    	if (Version.MODE == Version.MODE_SAMPLER)
		{
    		m_step = 0;
		}
        String line = null;
        while(m_isActive && ( (line = reader.readLine()) != null )) 
        {
        	if (Version.MODE == Version.MODE_SAMPLER)
			{
				System.out.println("output["+m_step+"]=\""+line.replaceAll("\"", "'")+"\";");
			}
        	synchronized (m_race)
        	{
        		if (!handle(line))
        		{
        			m_isActive = false;
        		}
        	}        	
        	if (Version.MODE == Version.MODE_SAMPLER)
    		{
        		m_step++;
    		}
        }
        if (Version.MODE != Version.MODE_SAMPLER)
		{
    		System.out.println("End");
		}
        m_bot.stop();
    }
    
    private boolean handle(String line)
    {
    	final int result = m_msgProcessor.handle(line);           	
		if (result == MsgProcessor.RESULT_OK)
		{
			final int cmd = m_bot.tick();
	    	if (Version.MODE == Version.MODE_SAMPLER)
			{
	    		System.out.println("input["+m_step+"]="+cmd+";");
			}
			if (cmd >= 0)        			
			{        				
				sendThrottle(cmd);
				m_race.setThrottle(cmd);
			}
			else if (cmd == Bot.CMD_SWITCH_LEFT)
			{        				
				sendSwitchLeft();
				m_race.switchLeft();
			}
			else if (cmd == Bot.CMD_SWITCH_RIGHT)
			{        				
				sendSwitchRight();
				m_race.switchRight();
			}
			else if (cmd == Bot.CMD_TURBO)
			{
				sendTurbo();
				m_race.useTurbo();
			}
			else
			{
				sendPing();
			}        	
		}
		else if (result == MsgProcessor.RESULT_PING)
		{
			sendPing();
		}
		else if (result == MsgProcessor.RESULT_IGNORE)
		{			
		}
		else
		{        					
			System.err.println("Invalid msg processor data. Giving up...");
			return false;
		}
		return true;
    }
}
