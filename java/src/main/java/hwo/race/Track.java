package hwo.race;

import hwo.analysis.Params;
import hwo.base.Version;
import hwo.message.GameInit.Data.TrackData;

public class Track {
	
	private Lane m_lanes[];
	  
	public Track(TrackData data)
	{		
		if (Version.DEBUG_SERVER)
		{
			System.out.println("Name"+ data.name);
			System.out.println("Num pieces:"+data.pieces.size());
			System.out.println("Num lanes:"+data.lanes.size());
		}
		
		double initialAngle = data.startingPoint.angle;
		m_lanes = new Lane[data.lanes.size()];
		for (int i = 0; i < data.lanes.size(); i++)
		{
			int index = data.lanes.get(i).index;
			int distanceFromCenter = data.lanes.get(i).distanceFromCenter; 
			m_lanes[index] = new Lane(initialAngle, distanceFromCenter, data.pieces);
		}
	}

	public Lane getLane(int index) 
	{
		return m_lanes[index];
	}
	

	public int getNumLanes() {
		return m_lanes.length;
	}

	public double getDist(CarState next, CarState prev) {
		
		if (next.pieceIndex == prev.pieceIndex)		
		{
			return next.piecePos - prev.piecePos;
		}
		else
		{
			Lane a = m_lanes[prev.lane];
			if (next.pieceIndex == ((prev.pieceIndex+1) % a.getNumPieces()))
			{
				return next.piecePos + 	
						(a.getPiece(prev.pieceIndex).length - prev.piecePos);					
			}
			else
			{
				Lane b = m_lanes[next.lane];
				return prev.piecePos + 	
						(b.getPiece(next.pieceIndex).length - next.piecePos);			
			}
		}	
	}

	
	private int getNextSwitchPos(int lane, int startPos)
	{
		int pos = startPos;
		do
		{
			pos = (pos + 1) % m_lanes[lane].getNumPieces();
			if (m_lanes[lane].getPiece(pos).laneSwitch != null)
			{
				return pos;
			}
		}
		while(pos != startPos);
		return startPos;
	}
	
	
	private double timeToReach(Params params, int lane, int a, int b, boolean isSwitching)
	{		
		double t = 0;
		if (isSwitching)
		{
			t += RaceInfo.SWITCH_LEN / (5000*params.getEnginePower());
		}
		while(a != b)
		{
			double len = m_lanes[lane].getPiece(a).length;
			double speed = 5000.0*params.getEnginePower();			
			t += len / speed;
			a = (a + 1) % m_lanes[lane].getNumPieces();			
		}
		return t;
	}

	public void updateLaneSwitches(Params params) 
	{
		final int numLanes = getNumLanes();
		final int numPieces = m_lanes[0].getNumPieces();
		for (int i = 0 ; i < numLanes; i++)
		{
			for (int j = 0; j < numPieces; j++)
			{
				LaneSwitch s = m_lanes[i].getPiece(j).laneSwitch;
				if (s != null)
				{
					int nextPos = getNextSwitchPos(i, j);
					for (int k = 0; k < 3; k++)
					{
						final int targetLane = i-1+k;
						if (targetLane < 0 || targetLane >= numLanes)
						{
							s.setSwitchTime(k, Double.MAX_VALUE);
						}
						else
						{
							double t = timeToReach(params, targetLane, j, nextPos, k != 1);
							// System.out.println(""+i+"-"+j+"-"+k+" T:"+t);
							s.setSwitchTime(k, t);
						}						
					}
				}
			}
		}
		
	}



}
