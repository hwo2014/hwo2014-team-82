package hwo.race;

import hwo.message.GameInit.Data.TrackData.Piece;

import java.util.List;

public class Lane 
{
	public class LanePiece
	{		
		public double length;
		public double startAngle;
		public double endAngle;
		public LaneSwitch laneSwitch = null;
		public double radius;
		
		private boolean isRightTurn;
		
		public boolean isRightTurn() 
		{
			return isRightTurn;
		}
	}
	
	private LanePiece m_lanePieces[];
	
	public LanePiece getPiece(int index)
	{
		return m_lanePieces[index];
	}
	
	public Lane(double angle, int distanceFromCenter, List<Piece> pieces) 
	{
		m_lanePieces = new LanePiece[pieces.size()];
		// float x = 0;
		// float y = 0;
		for (int i = 0; i < pieces.size(); i++)
		{
			Piece piece = pieces.get(i);
			
			m_lanePieces[i] = new LanePiece();
			m_lanePieces[i].startAngle = Math.toRadians(angle);
			if (piece.hasSwitch)
			{
				m_lanePieces[i].laneSwitch = new LaneSwitch();
			}
			m_lanePieces[i].radius = piece.radius;
			if (piece.length > 0 )
			{
				assert(m_lanePieces[i].radius == 0.0);
				m_lanePieces[i].length = piece.length;
				m_lanePieces[i].endAngle = m_lanePieces[i].startAngle;
				// x += Math.sin(m_lanePieces[i].endAngle)*piece.length;
				// y -= Math.cos(m_lanePieces[i].endAngle)*piece.length;			
			}
			else
			{
				if (piece.angle > 0)
				{
					m_lanePieces[i].isRightTurn = true;
					m_lanePieces[i].radius = piece.radius - distanceFromCenter;
				}
				else
				{
					m_lanePieces[i].radius = piece.radius + distanceFromCenter;
				}
				double arc = Math.abs(piece.angle) * 2.0 * Math.PI * m_lanePieces[i].radius / 360.0;
				m_lanePieces[i].length = arc;
				// System.out.println("RAD: "+ piece.radius+" ARC LEN:"+arc + " ANG:"+piece.angle);	
				
				angle += piece.angle;
				if ( angle > 360)
				{
					angle = angle - 360.0;
				}
				else if ( angle < 0)
				{
					angle = 360.0+angle;
				}
				m_lanePieces[i].endAngle = Math.toRadians(angle);				
							
				/*
				double xs = Math.sin(m_lanePieces[i].startAngle)*arc;
				double ys = Math.cos(m_lanePieces[i].startAngle)*arc; 
				double xe = Math.sin(m_lanePieces[i].endAngle)*arc;
				double ye = Math.cos(m_lanePieces[i].endAngle)*arc; 

				x += (xs + xe) / 2.0;
				y -= (ys + ye) / 2.0;
				*/
				/*
				int steps = (int)(arc);
				double stepLen = arc / (double)(steps);
				double tmpAngle = m_lanePieces[i].startAngle;
				double tmpStep = (m_lanePieces[i].endAngle-m_lanePieces[i].startAngle) / (double)(steps); 
				for (int j = 0; j < steps; j++)
				{
					tmpAngle += tmpStep;
					x += Math.sin(tmpAngle) * stepLen;
					y -= Math.cos(tmpAngle) * stepLen;				
				}*/

			}
						
			// System.out.println("POS X:"+x+" Y:"+y);	
		}
		
	}

	public int getNumPieces() {
		return m_lanePieces.length;
	}

}
