package hwo.race;

import hwo.message.CarPositions.CarPositionData;
import hwo.message.GameInit.Data.CarData;

public class LocalCar extends Car {
	
	private int m_throttle;
	
	final private int THROTTLE_DELAY = 0;
	
	private java.util.List<Integer> m_nextThrottle = new java.util.ArrayList<Integer>();
	
	private TurboInfo m_turbo;
	private TurboInfo m_nextTurbo;
	
	private int m_dir = 0;
	private int m_nextDir = 0;
	
	public LocalCar(CarData carData)
	{
		super(carData);
	}
	
	@Override
	public boolean isLocal()
	{
		return true;
	}

	public void setPosition(int tick, CarPositionData data) 
	{
		boolean isNewTick = m_carState == null || m_carState.tick != tick;
		if (isNewTick)
		{
			if (!m_nextThrottle.isEmpty())
			{
				m_throttle = m_nextThrottle.get(0);
				m_nextThrottle.remove(0);
			}
			m_dir = m_nextDir;
			m_turbo = m_nextTurbo;
		}
		super.setPosition(tick, data);
	
	}
		
	public int getThrottle()
	{
		return m_throttle;
	}
	
	public int getDir() 
	{
		return m_dir;
	}

	@Override
	public void setThrottle(int input) 
	{
		while (m_nextThrottle.size() < (THROTTLE_DELAY-1))
		{
			m_nextThrottle.add(m_throttle);
		}
		m_nextThrottle.add(input);
		while(m_nextThrottle.size() > THROTTLE_DELAY+1)
		{
			m_nextThrottle.remove(0);
		}
	}
	
	public void useTurbo(TurboInfo turbo)
	{
		m_nextTurbo = turbo;
	}	
	
	public void setDir(int i) {
		m_nextDir = i;
	}
}
