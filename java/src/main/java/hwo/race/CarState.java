package hwo.race;

public class CarState 
{
	public int lane;
	public int pieceIndex;	
	public double piecePos;
	
	public double vel;
	
	public double angle;
	public double angleVel;
	
	public int crashTicks;
	public int throttle;
	public int tick;
	public int dir;	
	
	public void set(CarState state)
	{
		lane = state.lane;
		pieceIndex = state.pieceIndex;
		piecePos = state.piecePos;		

		vel = state.vel;

		angle = state.angle;
		angleVel = state.angleVel;

		crashTicks = state.crashTicks;
		throttle = state.throttle;
		tick = state.tick;
		dir = state.dir;		
	}
}
