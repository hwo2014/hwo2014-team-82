package hwo.race;


import hwo.analysis.CarHistory;
import hwo.message.CarPositions.CarPositionData;
import hwo.message.GameInit.Data.CarData;
import hwo.message.Ident;


public class Car 
{


	protected boolean m_active = true;
		
	private CarData m_data;
	protected CarState m_carState;
	protected Race m_race;
	protected CarHistory m_carHistory = new CarHistory();
	
	private boolean m_hasSwitched;
	
	
	public Car(CarData carData)
	{
		m_data = carData;
		m_hasSwitched = false;
	}
	
	public CarState getCarState()
	{
		return m_carState;
	}
	public boolean isLocal()
	{
		return false;
	}
	

	public void start()
	{
		m_active = true;
		if (m_carState != null && m_carState.crashTicks > 0)
		{
			System.err.println("CRASH TICKS LEFT:"+m_carState.crashTicks);
		}
	}
	
	public void stop()
	{
		m_active = false;
	}
	
	public void update()
	{	
	}

	public void setThrottle(int input) 
	{
		assert(false);
	}

	public void setDir(int i) 
	{
	}
	

	public boolean hasMatchingId(Ident id) 
	{
		return m_data.id.hasMatchingId(id);
	}
	
	public int getThrottle()
	{
		return 0;
	}
	
	public int getDir()
	{
		return 0;
	}

	public void setPosition(int tick, CarPositionData data) 
	{
	
		// int lastPieceIndex = m_pieceIndex;
		// double lastPiecePos = m_piecePos;
		CarState prev = m_carState;
		
		m_carState = new CarState();
		m_carState.angle = data.angle;		
		m_carState.lane = data.piecePosition.lane.endLaneIndex;
		m_carState.piecePos = data.piecePosition.inPieceDistance;
		m_carState.pieceIndex = data.piecePosition.pieceIndex;
		m_carState.throttle = getThrottle();
		
		m_carState.tick = tick;
		m_carState.dir = getDir();
		
		if (prev != null)
		{
			m_carState.vel = Math.abs(m_race.getTrack().getDist(m_carState, prev));
			if (m_carState.pieceIndex != prev.pieceIndex)
			{
				if (m_hasSwitched)
				{
					m_hasSwitched = false;
					m_carState.vel += RaceInfo.SWITCH_LEN;
				}
			}
			if (m_carState.lane != prev.lane)
			{
				m_hasSwitched = true;
			}
			m_carState.angleVel = m_carState.angle - prev.angle;
			if (!m_active && prev.crashTicks == 0)
			{
				m_carState.crashTicks = RaceInfo.CRASH_TICKS;
			}
			if (prev.crashTicks > 0)
			{
				m_carState.crashTicks = prev.crashTicks -1;
			}
		}

		m_carHistory.addState(m_carState);

		/*
		
		m_lane = data.piecePosition.lane.endLaneIndex;
		m_pieceIndex = data.piecePosition.pieceIndex;
		m_piecePos = data.piecePosition.inPieceDistance;
		
		double diff;
		if (m_pieceIndex == lastPieceIndex)		
		{
			diff = m_piecePos - lastPiecePos;
		}
		else if (m_pieceIndex == ((lastPieceIndex+1) % m_track.getLane(m_lane).getNumPieces()))
		{
			diff = m_piecePos + 	
				(m_track.getLane(m_lane).getPiece(lastPieceIndex).length - lastPiecePos);					
		}
		else
		{
			diff = lastPiecePos + 	
					(m_track.getLane(m_lane).getPiece(m_pieceIndex).length - m_piecePos);			
		}
		
		double angleDiff = data.angle - m_angle;
		m_angle = data.angle;
		totalError += Math.abs(diff) + Math.abs(angleDiff);
		
		System.out.println("DIFF: "+ diff + " total:"+totalError+ " ANG:"+ 
				(m_track.getLane(m_lane).getPiece(m_pieceIndex).endAngle - 
				m_track.getLane(m_lane).getPiece(m_pieceIndex).startAngle) + " THROTTLE:"+m_throttle + "  ANG DIFF:"+angleDiff);
		
		// System.out.println("POS:"+m_piecePos+"/"+m_track.getLane(m_lane).getPiece(m_pieceIndex).length + " ANG:"+data.angle);
		*/
	}

	public CarHistory getHistory() 
	{
		return m_carHistory;
	}

	public double getAngle() {
		if (m_carState != null)
		{
			return m_carState.angle;
		}
		else
		{
			return 0.0;
		}
		
	}

	public double getSpeed() {
		return m_carState.vel;
	}

	public void setRace(Race race) {
		m_race = race;
		
	}

	public Race getRace()
	{
		return m_race;
	}

	public void useTurbo(TurboInfo m_turbo) 
	{
		
	}




}
