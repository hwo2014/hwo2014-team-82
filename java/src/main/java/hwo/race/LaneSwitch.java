package hwo.race;

import hwo.base.Bot;

public class LaneSwitch 
{
	public final static int LEFT = 0;
	public final static int STRAIGHT = 1;
	public final static int RIGHT = 2;
	
	private double m_value[] = new double[3];
	
	public void setSwitchTime(int k, double value) 
	{
		m_value[k] = value;		
	}
	
	public double getSwitchTime(int k)
	{
		return m_value[k];
	}
	
	public int getFastest()
	{
		if (m_value[STRAIGHT] <= m_value[RIGHT])
		{
			if (m_value[STRAIGHT] <= m_value[LEFT])
			{
				return STRAIGHT;
			}
			else
			{
				return LEFT;
			}
		}
		else if (m_value[RIGHT] < m_value[LEFT])
		{
			return RIGHT;
		}
		else
		{
			return LEFT;
		}
	}
	
}
