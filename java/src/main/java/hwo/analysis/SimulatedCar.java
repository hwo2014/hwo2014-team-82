package hwo.analysis;

import hwo.base.Bot;
import hwo.race.Car;
import hwo.race.CarState;
import hwo.race.RaceInfo;
import hwo.race.Lane;
import hwo.race.Lane.LanePiece;
import hwo.util.HWOMath;

public class SimulatedCar 
{
	public final static boolean DEBUGGING = false;
	
	private Params m_params;
	
	protected boolean m_hasFailed;
	
	private double m_risk = 1.0;
	
	private CarState m_simulatedState = new CarState();
	
	protected boolean m_hasSwitched;
	
	private Car m_car;
		
	public SimulatedCar(Params params, Car car)
	{		
		m_params = params;
		m_car = car;	
	}
	
	public void setRisk(double risk) 
	{
		if (m_car.isLocal())
		{
			m_risk = risk;
		}
		else
		{
			m_risk = 1.0;
		}
	}
	
	public boolean initSimulation()
	{
		if (m_car.getHistory().hasStateInfo())
		{
			m_car.getHistory().onAnalysisStarted();
			return true;
		}
		return false;
	}
	
	public void startSimulation()
	{
		m_hasFailed = false;
		m_hasSwitched = false;	
		m_car.getHistory().resetAnalysis();
		CarState state = m_car.getHistory().peekNextAnalysisState();
		setSimulatedState(state);
	}

	public void setSimulatedState(CarState state)
	{
		m_simulatedState.tick = state.tick;
		m_simulatedState.vel = state.vel;
		m_simulatedState.angle = state.angle;
		m_simulatedState.angleVel = state.angleVel;
		m_simulatedState.lane = state.lane;
		m_simulatedState.pieceIndex = state.pieceIndex;
		m_simulatedState.piecePos = state.piecePos;		
		m_simulatedState.crashTicks = state.crashTicks;
		if (m_car.isLocal())
		{
			m_simulatedState.throttle = state.throttle;
			m_simulatedState.dir = state.dir;
		}
		else
		{
			m_simulatedState.throttle = 0;
			m_simulatedState.dir = 0;			
		}
	}
	
	public void stopSimulation()
	{

	}

	public void deinitSimulation()
	{
		m_car.getHistory().onAnalysisEnded();
	}
	
	public boolean simulate()
	{
	/*
		CarState prev = null;
		CarState state =  m_car.getHistory().getNextAnalysisState();
		while (state != null)
		{
			if (prev != null)
			{
				final Lane lane = m_car.getTrack().getLane(state.lane);
				final LanePiece piece = lane.getPiece(state.pieceIndex);		
				double angleAcc = state.angleVel - prev.angleVel;
			if (piece.radius == 0)
			{
					System.out.print(state.throttle+";"+state.vel+ ";"+ 
						 (state.vel - prev.vel) + 
						";"+ state.angle + ";"+state.angleVel);
					
					System.out.print( ";"+angleAcc);
					System.out.println("");
			}
			}
			prev = state;
			state =  m_car.getHistory().getNextAnalysisState();
		}
		return false;
		*/
		CarState state =  m_car.getHistory().getNextAnalysisState();
		if (state !=null)
		{
			
			// System.out.println("Simulating tick: "+state.tick);
			
			m_simulatedState.throttle = state.throttle;
			m_simulatedState.dir = state.dir;
			simulateTick();
			calculateScore(state);
			return !m_hasFailed;
		}
		else
		{
			return false;
		}
	}
	
	public void calculateScore(CarState state)
	{
		double diff = m_car.getRace().getTrack().getDist(state, m_simulatedState);
		diff = Math.abs(diff);
		
		/*if (DEBUGGING)
		{
			System.out.println("VEL:"+ HWOMath.round2(state.vel)+ 
			 	" vs:"+ HWOMath.round2(m_simulatedState.vel)+
				" PIECEPOS:"+HWOMath.round2(state.piecePos)+ 
				" POS:"+ HWOMath.round2(m_simulatedState.piecePos)); 
		}*/

		double angleDiff = Math.abs(m_simulatedState.angle-state.angle);
		// angleDiff += Math.abs(m_simulatedState.angleVel-state.angleVel);
		/* if (state.angle > 0)
		{
			if (m_simulatedState.angle > state.angle)
			{
				angleDiff *= 4;
			}
		}
		else
		{
			if (m_simulatedState.angle < state.angle)
			{
				angleDiff *= 4;
			}
		}*/
		
		diff += angleDiff*10;
		diff = Math.sqrt(diff+1.0);
		if (DEBUGGING /*&& diff > 0.1*/)
		{
		   System.out.print("["+state.tick+"]"+HWOMath.round2(state.piecePos)+"-"+
				HWOMath.round2(m_simulatedState.piecePos)+ " THROT:"+ state.throttle+ " DIFF: "
				+ ""+diff+ " vel:"+
				HWOMath.round2(state.vel)+"("+state.lane+") vs "+
				HWOMath.round2(m_simulatedState.vel)+"("+m_simulatedState.lane+")"); 
			
		    
			final Lane lane = m_car.getRace().getTrack().getLane(state.lane);
			final LanePiece piece = lane.getPiece(state.pieceIndex);		
			
			/*if (state.angle != 0)
			{*/
				System.out.print(  " angle: "+ 
						HWOMath.round3(state.angle) + "("+
						HWOMath.round3(state.angleVel)+") - "+
						HWOMath.round3(m_simulatedState.angle)+"("+
						HWOMath.round3(m_simulatedState.angleVel)+") "+ piece.radius+ " ");
			 //}
				
				if (state.crashTicks != 0 || m_simulatedState.crashTicks != 0)
				{
					System.out.print( "Crash:"+state.crashTicks+ "-"+m_simulatedState.crashTicks);
				}
			
			System.out.println("");
		}
		
		
		if (diff > 0)
		{
			m_params.addScore(diff);
			if (DEBUGGING)
			{
				System.out.println("---------------- CORRECTING ------------ ("+
					HWOMath.round2(m_params.getScore())+")");
			}
			// setSimulatedState(state);
			
		}
		if (diff > 10.0)
		{
			// m_hasFailed = true;
		}

	}
	
	public void printDebug()
	{
		System.out.print(
			"["+m_simulatedState.tick+"] "+ 
			"@"+m_simulatedState.pieceIndex + "/"+HWOMath.round2(m_simulatedState.piecePos) + 
			"("+m_simulatedState.lane+") "+
			"Speed: "+ HWOMath.round2(m_simulatedState.vel));
		if (m_simulatedState.angle != 0)
		{
			System.out.print("Angle: "+ HWOMath.round2(m_simulatedState.angle)+ " ^"+HWOMath.round2(m_simulatedState.angleVel));
		}
		System.out.println("");
	}
	
	public void runCommand(int cmd)
	{
		if (cmd >= 0)
		{
			m_simulatedState.throttle = cmd;
		}
		else if (cmd == Bot.CMD_SWITCH_LEFT)
		{
			m_simulatedState.dir = -1;
		}
		else if (cmd == Bot.CMD_SWITCH_RIGHT)
		{
			m_simulatedState.dir = -2;
		}
	}
	
	private double getSimulatedThrottle()
	{
		 return (double)(m_simulatedState.throttle) * m_params.getEnginePower();
	}
	
	public void simulateTick()
	{
		if (m_simulatedState.crashTicks == 0)
		{
			// int m_subSteps = 1;
			// for (int i = 0; i < m_subSteps; i++)
			// {
				final Lane lane = m_car.getRace().getTrack().getLane(m_simulatedState.lane);
				final LanePiece piece = lane.getPiece(m_simulatedState.pieceIndex);		
				
				double pieceLen = piece.length;
				if (m_hasSwitched)
				{
					pieceLen += RaceInfo.SWITCH_LEN;
				}
			
				final double acc = getSimulatedThrottle() - (m_simulatedState.vel * (m_params.getFriction()*2.0));					
				m_simulatedState.vel += acc;

				simulateAngle(piece, pieceLen, acc);
				boolean isCrashTick=
						// (piece.radius > 0) &&
						(((m_simulatedState.angle > 0 && m_simulatedState.angleVel > 0) ||
						(m_simulatedState.angle < 0 && m_simulatedState.angleVel < 0))) &&
						((Math.abs(m_simulatedState.angle) >= RaceInfo.CRASH_LIMIT*m_risk));

				if (!isCrashTick)
				{
					m_simulatedState.piecePos += m_simulatedState.vel;
					
					if (m_simulatedState.piecePos >= pieceLen)
					{
						m_simulatedState.piecePos -= pieceLen;
						m_simulatedState.pieceIndex = (m_simulatedState.pieceIndex + 1) % lane.getNumPieces();												
						final LanePiece nextPiece = lane.getPiece(m_simulatedState.pieceIndex);
						m_hasSwitched = false;
						if (nextPiece.laneSwitch != null)
						{
							if (m_simulatedState.dir != 0)
							{
								int origLane = m_simulatedState.lane; 
								m_simulatedState.lane += m_simulatedState.dir;
								m_simulatedState.dir = 0;
								if (m_simulatedState.lane < 0)
								{
									m_simulatedState.lane = 0;
								}
								else if (m_simulatedState.lane >= m_car.getRace().getTrack().getNumLanes())							
								{
									m_simulatedState.lane = m_car.getRace().getTrack().getNumLanes() - 1;
								}
								m_hasSwitched = origLane != m_simulatedState.lane;
							}
						}
					}
				}
				else
				{
					if (DEBUGGING)
					{
						System.out.println("CRASHED AT "+m_simulatedState.angle);
					}

					m_simulatedState.vel = 0;
					m_simulatedState.angle = 0;
					m_simulatedState.angleVel = 0;
					m_simulatedState.crashTicks = RaceInfo.CRASH_TICKS;
				}
				
		}
		else
		{
			m_simulatedState.crashTicks--;
		}
		m_simulatedState.tick++;
	}
	
	
	private void simulateAngle(final LanePiece piece, double pieceLen, double acc)
	{
		double angleAcc = 0;
		if (piece.radius > 0)
		{
			// double THRESHOLD = Math.sqrt(0.322*piece.radius+(m_simulatedState.angle/2));
			double THRESHOLD = Math.sqrt(0.322*piece.radius);
			if (m_simulatedState.vel > THRESHOLD)
			{ 
				// angleAcc = 267*(m_simulatedState.vel-THRESHOLD)/THRESHOLD * 0.0995; //m_params.getAngleMulti();
				
				// angleAcc = 200*Math.pow((m_simulatedState.vel)/THRESHOLD-1.0, m_params.getAngleMulti()*10+1.0);
				angleAcc = 200*((m_simulatedState.vel)/THRESHOLD-1.0) * m_params.getAngleMulti();
			}
			
			if (!piece.isRightTurn())
			{
				angleAcc = -angleAcc;
			}
		}

		double target = angleAcc-(m_simulatedState.angle*m_params.getAngleAccMulti());
		angleAcc = (target - m_simulatedState.angleVel)*10*m_params.getAngleVelMulti();

		m_simulatedState.angleVel += angleAcc;
		m_simulatedState.angle += m_simulatedState.angleVel;
	}

	public CarState getSimulatedState() 
	{
		return m_simulatedState;
	}

	public Car getSource() 
	{
		return m_car;
	}

	
}
