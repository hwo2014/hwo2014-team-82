package hwo.analysis;

import hwo.race.CarState;

public class CarHistory 
{

	private final int MAX_STATES = 3000;
	private CarState m_states[] = new CarState[MAX_STATES];
	private int m_currentStateIndex;
	private int m_analysisStateIndex;
	private int m_firstState;
	private int m_lastState;
	private boolean m_isLocked = false;
	private boolean m_isStalled;
	
	public CarHistory()
	{
		reset();
	}
	
	public void addState(CarState state)
	{	
		if (!m_isStalled)
		{
			m_currentStateIndex = (m_currentStateIndex + 1) % MAX_STATES;
			if (m_isLocked)
			{
				if (m_currentStateIndex == m_firstState)
				{
					System.out.println("CarHistory stalled");
					m_isStalled = true;					
				}
				else
				{
					m_states[m_currentStateIndex] = state;
				}
			}
			else
			{
				m_states[m_currentStateIndex] = state;
				// System.out.println("ADD STATE "+ m_firstState+ " - "+m_lastState);
				if (m_currentStateIndex == m_firstState && m_lastState != -1)
				{
					m_firstState = (m_firstState + 1) % MAX_STATES;
				}
				m_lastState = m_currentStateIndex;
			}
		}
	}
	
	public void resetAnalysis()
	{
		m_analysisStateIndex = m_firstState;
	}
	
	public CarState peekNextAnalysisState()
	{
		return m_states[m_analysisStateIndex];
	}
	
	public CarState getNextAnalysisState()
	{
		if (m_analysisStateIndex == m_lastState)
		{
			return null;
		}
		m_analysisStateIndex = (m_analysisStateIndex + 1) % MAX_STATES;
		return m_states[m_analysisStateIndex];
	}
	
	public boolean hasStateInfo()
	{
		return m_lastState != -1;
	}
	
	public void onAnalysisStarted()
	{
		m_isLocked = true;
	}
	
	public void onAnalysisEnded()
	{
		m_isLocked = false;
		if (m_isStalled)
		{
			reset();
		}
		else
		{
			m_lastState = m_currentStateIndex;
		}
	}
	
	public void reset()
	{
		m_isStalled = false;
		m_currentStateIndex = 0;
		m_firstState = 1;
		m_lastState = -1;		
	}
}
