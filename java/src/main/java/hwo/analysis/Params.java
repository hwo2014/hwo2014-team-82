package hwo.analysis;

public class Params {


	private final static int VALUE_SCORE = 0;
	private final static int VALUE_ENGINE = 1;
	private final static int VALUE_FRICTION = 2;
	private final static int VALUE_ANGLE_MULTI = 3;
	private final static int VALUE_ANGLE_VEL_MULTI = 4;
	private final static int VALUE_ANGLE_ACC_MULTI = 5;

	/**
	 * N Param values
	 * [0] = Score
	 * [1-N/2] = Values
	 * [N/2-N] = Evolution frequency
	 */
	private double m_values[] = {
			4481.42002385041,
			0.5,
			0.01,
			0.11804378396435063,
			0.009283487390342656,
			0.0978518190708589,
			0.1,
			0.1,
			0.55,
			0.1,
			1.0,
			};
	/*
	private double m_values[] = {
			21131.74, // 4481,
			0.5,

			0.01,

			0.11800580342010403,

			0.009278842828617165,
			0.0978518190708589,
			
			0.0,
			0.0,
			1.0,
			1.0,
			1.0,
		};*/
	/*
	private double m_values[] = {
			4.087537555539884,
			0.5,
			0.01,
			0.17183515027249446,
			0.823822557926178,
			0.0978518190708589,
			0.1,
			0.1,
			0.991813691047366,
			0.991813691047366,
			1.0,
			};*/
	
	/*
	  
	 	private double m_values[] = {
			10000.437, // 4481,
			0.5,

			0.01,

			0.11800580342010403,

			0.009278842828617165,
			0.0978518190708589,
			
			0.0,
			0.0,
			1.0,
			1.0,
			1.0,
		};
	  
	m_bestSetting[0]=4481.559910674517;
	m_bestSetting[1]=0.5;
	m_bestSetting[2]=0.01;
	m_bestSetting[3]=0.11800580342010403;
	m_bestSetting[4]=0.009278842828617165;
	m_bestSetting[5]=0.0978518190708589;
*/


	/* Good setup!
	m_bestSetting[1]=0.5;
	m_bestSetting[2]=0.01;
	m_bestSetting[3]=0.10160602172931386;
	m_bestSetting[4]=0.010679868210338492;
	m_bestSetting[5]=0.06778451524506303;
	m_bestSetting[6]=0.548020253429989;
	 */
	
	public Params()
	{
		
	}
	
	public Params(Params in)
	{
		final int offset = (m_values.length-1)/2;
		// Loop until changed values
		boolean bOriginal;
		m_values[0] = 0;
		do
		{
			bOriginal = true;
			for (int i = 1; i < offset; i++)
			{
				m_values[i] = in.getValue(i);
				if (Math.random() < m_values[i+offset])
				{
					m_values[i+offset] = (1.0-in.getValue(i+offset)) / 2 + in.getValue(i+offset);					
					if (Math.random() < 0.05)
					{
						m_values[i] = (float)Math.random();
					}
					else
					{
						double part = m_values[i]  * 0.2f + 0.0001f;
						m_values[i] = m_values[i] + 2*(Math.random()*part - (part/2));
						m_values[i] = Math.max(0.0, m_values[i]);
						m_values[i] = Math.min(1.0, m_values[i]);
					}
					bOriginal = false;
				}
				else
				{
					m_values[i+offset] = in.getValue(i+offset);
				}
				// Update evolve frequency
				in.setValue(i+offset, Math.max(0.1, in.getValue(i+offset)*0.999));
			}
		} while (bOriginal);
	}
	
	public void setValue(int index, double value)
	{
		m_values[index]=value;
	}
	
	public double getValue(int index)
	{
		return m_values[index];
	}
	
	public double getScore()
	{
		return m_values[VALUE_SCORE];	
	}

	public double getEnginePower()
	{
		return m_values[VALUE_ENGINE] * 0.0004;	
	}
	
	public double getFriction()
	{
		return m_values[VALUE_FRICTION];	
	}

	public double getAngleMulti()
	{
		return m_values[VALUE_ANGLE_MULTI];	
	}
	
	public double getAngleVelMulti()
	{
		return m_values[VALUE_ANGLE_VEL_MULTI];	
	}

	public double getAngleAccMulti()
	{
		return m_values[VALUE_ANGLE_ACC_MULTI];	
	}
	
	public void print()
	{
		System.out.println("private double m_values[] = {");
		for (int i = 0; i < m_values.length; i++)
		{
			System.out.println(""+m_values[i]+",");
		}
		System.out.println("};");
	}

	public void addScore(double diff) 
	{
		m_values[VALUE_SCORE] += diff;
	}

	public void set(Params m_params) 
	{
		for (int i = 0; i < m_values.length; i++)
		{
			m_values[i] = m_params.getValue(i);
		}
		
	}	
}
