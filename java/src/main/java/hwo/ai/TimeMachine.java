package hwo.ai;

import hwo.analysis.Params;
import hwo.analysis.SimulatedCar;
import hwo.base.Bot;
import hwo.debug.Debug;
import hwo.race.Car;
import hwo.race.CarState;
import hwo.race.LaneSwitch;
import hwo.race.Race;
import hwo.race.Lane.LanePiece;

public class TimeMachine extends Simulator
{
	private CarState m_initialState[];

	private int m_startTick;
	private int m_crashTick;
	
	private TickCommandBuffer m_testBuffer = new TickCommandBuffer();
	private TickCommandBuffer m_targetBuffer;
	private double m_posRadius[] = new double[TickCommandBuffer.MAX_COMMANDS];
	private double m_posVel[] = new double[TickCommandBuffer.MAX_COMMANDS];
	private LaneSwitch m_posSwitch[] = new LaneSwitch[TickCommandBuffer.MAX_COMMANDS]; 
	
	private final static int TICK_SAFETY_POST = 10;
	private final static int TICK_SAFETY_PRE = 1;
	private double m_bestScore;

	public TimeMachine(TickCommandBuffer cmdBuffer, Race race)
	{
		super(race);
		m_targetBuffer = cmdBuffer;
	}
	
	public void begin()
	{
		m_nextAnalysisTick = -1;
		super.begin();
		m_initialState = new CarState[m_cars.length];
		for (int i = 0; i < m_cars.length; i++)
		{
			m_initialState[i] = new CarState();
		}		
	}

	protected void setupCars()
	{
		for (int i = 0; i < m_cars.length; i++)
		{			
			Car realCar = m_race.getCars()[i];
			m_initialState[i].set(realCar.getCarState());
		}	
	}
	
	@Override
	protected boolean checkNextTick()
	{
		boolean hasTickChanged = false;
		synchronized (m_race)
		{
			m_startTick = m_race.getTick()+1;
			if (m_startTick >= m_nextAnalysisTick)
			{
				m_params.set(m_race.getInfo().getParams());
				setupCars();
				m_nextAnalysisTick = m_startTick+1;
				hasTickChanged = true;
				
			}
		}
		return hasTickChanged;
	}
	
	protected void analyse() 
	{
		for (int i = 0; i < m_cars.length; i++)
		{
			m_cars[i].setRisk(0.982);//90);//0.90);//88);//88);
		}
		
		int tick = m_startTick;
		int endTick = tick+TickCommandBuffer.MAX_COMMANDS-TICK_SAFETY_POST;
		
		while( tick < m_startTick + TICK_SAFETY_PRE)
		{
			m_testBuffer.setCommand(tick, m_targetBuffer.getCommand(tick));
			tick++;
		}
		
		for (int i = m_startTick + TICK_SAFETY_PRE; i < endTick; i++)
		{
			m_testBuffer.setCommand(i, 1000);
		}
		
		double score = testRun();		
// 		checkSwitching(endTick);		
		testRun();
		// System.out.println("SCORE:"+score);
		
		if (m_crashTick != -1 && m_crashTick > m_startTick)
		{		
			int lowEdge = m_startTick;
			int maxThrottle = 1000;
			while (m_crashTick != 1 && maxThrottle >= 0)
			{
				lowEdge = findLowEdge(m_crashTick, endTick, maxThrottle);
				maxThrottle -= 100;
			}			
			
			if (m_crashTick != -1)
			{
				Debug.error(lowEdge >= m_startTick+TICK_SAFETY_PRE, "Find low edge failed at "+lowEdge+" start:"+m_startTick);
				findHighEdge(lowEdge, endTick-1);
			}
		}
		
		if (Debug.ENABLED)
		{
			for (int i = m_startTick; i < m_startTick + TICK_SAFETY_PRE; i++)
			{
				Debug.error(m_testBuffer.getCommand(i) == m_targetBuffer.getCommand(i),
					"Target Buffer Corrupted @ "+i+ " start:"+m_startTick);
			}
		}
		
		m_targetBuffer.set(m_testBuffer);
		
		if (Debug.ENABLED)
		{
			int timeUsed = m_startTick;
			synchronized (m_race)
			{
				timeUsed = m_race.getTick()+1-timeUsed;
			}
			Debug.error(timeUsed <= 1, "Time machine used ticks:"+(timeUsed));
		}
	}
	
	private int getNextSwitchPos( int start, int end)
	{
		for (int i = start; i < end; i++)
		{
			LaneSwitch s = m_posSwitch[(i+2) % TickCommandBuffer.MAX_COMMANDS];
			if (s != null)
			{
				return i;
			}
		}
		return -1;
	}
	/*
	private void checkSwitching(int endTick)
	{
		int prevCmd = m_testBuffer.getCommand(m_startTick + TICK_SAFETY_PRE-1);
		int pos = m_startTick + TICK_SAFETY_PRE;
		while(pos < endTick)
		{
			int nextPos = getNextSwitchPos(pos, endTick);			
			if (nextPos != -1)
			{
				int result = checkSwitch(1000, nextPos);
				if (result != LaneSwitch.STRAIGHT)
				{
					for (int i = pos; i < nextPos; i++)
					{
						int nextCmd = m_testBuffer.getCommand(i);
						if (nextCmd == prevCmd)
						{
							if (result == LaneSwitch.LEFT)
							{
								m_testBuffer.setCommand(i, Bot.CMD_SWITCH_LEFT);
							}
							else
							{
								m_testBuffer.setCommand(i, Bot.CMD_SWITCH_RIGHT);
							}
						}
						else
						{
							prevCmd = nextCmd;
						}					
					}
				}
				pos = nextPos;
			}
			else
			{
				pos = endTick;
			}
		}
	}
	
	private int checkSwitch(int maxThrottle, int pos)
	{		
		int result = LaneSwitch.STRAIGHT;
		LaneSwitch s = m_posSwitch[(pos+2) % TickCommandBuffer.MAX_COMMANDS];
		if (s != null)
		{
			result = s.getFastest();
		}
		return result;
	}*/
	
	private int findLowEdge(int tick, int endTick, int maxThrottle)
	{	
		while(tick > m_startTick+TICK_SAFETY_PRE && m_crashTick != -1)
		{
			tick--;
			for (int i = tick; i < endTick; i++)
			{
				if (m_posRadius[(i) % TickCommandBuffer.MAX_COMMANDS] == 0)
				{
					m_testBuffer.setCommand(i, 0);
				}
				else
				{
					int throttle;
					double targetSpeed = Math.sqrt(0.322 * m_posRadius[(i) % TickCommandBuffer.MAX_COMMANDS]);
					double curSpeed = m_posVel[(i) % TickCommandBuffer.MAX_COMMANDS];
					if (curSpeed > 0)
					{
						throttle = (int)Math.min(maxThrottle, (targetSpeed*targetSpeed*100 / curSpeed));					
					}
					else
					{
						throttle = maxThrottle;
					}					
					m_testBuffer.setCommand(i, throttle);
				}				
			}
			// checkSwitching(endTick);
			m_bestScore = testRun();
		// 	System.out.println("SCORE:"+score + " LowEdge @"+tick);
		}
		return tick;
	}
	
	private boolean testTick(int tick, int throttle)
	{
		int orig = m_testBuffer.getCommand(tick); 
		m_testBuffer.setCommand(tick, throttle);
		
		double prevScore = m_bestScore;
		double score = testRun();
		// System.out.println("SCORE:"+score + " HighEdge @"+edgeTick);
		if (m_crashTick != -1 || score < prevScore)
		{
			m_bestScore = prevScore;
			m_testBuffer.setCommand(tick, orig);
			return false;
		}
		return true;
	}
	
	private int findHighEdge(int startTick, int endTick)
	{
		int tick = endTick; // tick;
		while(tick >= startTick)// startTick /* endTick*/)
		// int tick = startTick;
		// while(tick < endTick)
		{		
			int src = m_testBuffer.getCommand(tick);
			// Debug.error(m_testBuffer.getCommand(tick) == 0, "Invalid command:"+m_testBuffer.getCommand(tick)+ 
				// " tick:"+tick+ " start:"+startTick);
			
			if (src >= 0 && src < 1000)
			 {
			int part = (1000-src) / 10;
			for (int i = 1; i < 10; i++)
			{
				if (!testTick(tick, src + i*part))
				{
					break;
				}
			}
			 }
			// }
			/*
			if (!testTick(tick, 1000))
			{
				int max = 999;
				int min = 1;
				for (int i = 0; i < 10;i++)
				{
					int cur = (max+min) / 2;
					if (testTick(tick, cur))
					{
						min = cur;
					}
					else
					{
						max = cur;
					}
				}
				
				if (min > 0)
				{
					if (!testTick(tick, max))
					{
						if (!testTick(tick, min))
						{
							testTick(tick, 1);
							// System.out.println("MIN:"+min);	
						}
					}
					else
					{
					 	// System.out.println("MAX:"+max);
					}
				}
			} */
			tick--;
			// tick++;
		}

		return tick;
	}
	
	private double testRun()
	{
		m_crashTick = -1;
		for (int i = 0; i < m_cars.length; i++)
		{
			m_cars[i].setSimulatedState(m_initialState[i]);
		}
		int tick = m_startTick;
		int endTick = tick+TickCommandBuffer.MAX_COMMANDS-TICK_SAFETY_POST-1;
		double score = 0;		
		do
		{
			for (int i = 0; i < m_cars.length; i++)
			{
				final SimulatedCar car = m_cars[i];
				car.simulateTick();
				// m_cars[i].printDebug();
				if (m_race.isMyCar(car.getSource()))
				{
					score += car.getSimulatedState().vel;
					car.runCommand(m_testBuffer.getCommand(tick));				
					if (car.getSimulatedState().crashTicks > 0)
					{
						score = 0;
						tick = endTick;
						m_crashTick = tick;
					}
					LanePiece p = m_race.getTrack().getLane(car.getSimulatedState().lane).
							getPiece(car.getSimulatedState().pieceIndex);
					final int bufferPos = tick % TickCommandBuffer.MAX_COMMANDS;
					m_posRadius[bufferPos] = p.radius;
					m_posVel[bufferPos] = car.getSimulatedState().vel;

					// System.out.println("!!!!!!!!!!!!!!!"+car.getSimulatedState().pieceIndex);
					m_posSwitch[bufferPos] = p.laneSwitch;					
				}
			}
			tick++;
		} while(tick < endTick);
		return score;
	}
}
