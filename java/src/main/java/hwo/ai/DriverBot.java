package hwo.ai;

import hwo.base.Bot;
import hwo.race.Race;
import hwo.race.RaceInfo;
import hwo.race.RaceListener;

public class DriverBot extends Bot implements RaceListener
{
	final private TimeMachine m_timeMachine;
	final private ConfigFinder m_configFinder;
	final private TickCommandBuffer m_cmdBuffer = new TickCommandBuffer();

	public DriverBot(Race race)
	{
		super(race);
		m_configFinder = new ConfigFinder(race);
		m_timeMachine = new TimeMachine(m_cmdBuffer, race);				
	}
	@Override
	public int tick() 
	{
		// Normally bot uses buffered response, but if getting close 
		// to limit there is some instant response
		double estimatedAngle = m_race.getAngle()+(m_race.getCarState().angleVel*1);
		if (Math.abs(estimatedAngle) >= RaceInfo.CRASH_LIMIT)
		{			
			m_cmdBuffer.setCommand(m_race.getTick()+1,0); 
				// (int)((1 - (RaceInfo.CRASH_LIMIT / 
				// Math.abs(m_race.getCars()[0].getAngle())))*1000));
		}
		return m_cmdBuffer.getCommand(m_race.getTick()+1);
	}
	
	public void start()
	{
		m_timeMachine.waitForDone();
		m_configFinder.waitForDone();

		m_race.addListener(this);
		m_timeMachine.init();
		m_configFinder.init();
	}
	
	public void stop()
	{
		m_race.removeListener(this);

		stopHelpers();

		m_configFinder.waitForDone();
		m_configFinder.deinit();
		m_timeMachine.waitForDone();
		m_timeMachine.deinit();
	}
	
	public void onRaceStarted() 
	{
		startHelpers();
	}
	
	public void onRaceEnded() 
	{	
		stopHelpers();
	}
	
	private void startHelpers()
	{
		m_configFinder.begin();
		m_timeMachine.begin();	
	}
	
	private void stopHelpers()
	{
		m_configFinder.end();
		m_timeMachine.end();
	}
	
}
