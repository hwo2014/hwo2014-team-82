package hwo.ai;

import hwo.analysis.Params;
import hwo.analysis.SimulatedCar;
import hwo.debug.Debug;
import hwo.race.Car;
import hwo.race.Race;

public abstract class Simulator extends Thread
{	
	protected Params m_params = new Params(); 
	protected int m_nextAnalysisTick = -1;
	protected boolean m_isActive;
	final protected Race m_race;	
	protected SimulatedCar m_cars[];
	
	public Simulator(Race race)
	{
		m_race = race;
	}
	
	public void begin()
	{
		m_cars = new SimulatedCar[m_race.getCars().length];
		for (int i = 0; i < m_cars.length; i++)
		{
			Car realCar = m_race.getCars()[i];
			m_cars[i] = new SimulatedCar(m_params, realCar);
		}
	
		m_isActive = true;		
		start();
	}

	public void end()
	{
		m_isActive = false;
	}
	
	public void init()
	{

	}
	
	public void deinit()
	{
		m_cars = null;
	}
		
	
	abstract protected boolean checkNextTick();
	abstract protected void analyse();
	
	public void run()
	{
		while (m_isActive)
		{
			if (checkNextTick())
			{
				analyse();
				yield();
			}
			else
			{
				try
				{
					// TODO: Replace polling with notify...
					sleep(1);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public void waitForDone()
	{
		try 
		{
			Debug.error(!m_isActive, "Thread not stopped");
			System.out.println("Waiting for "+this.toString()+"...");
			join();			
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
}
