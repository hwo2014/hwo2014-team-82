package hwo.ai;


/**
 * Ring buffer for bot commands. 
 */
public class TickCommandBuffer 
{
	
	final public static int MAX_COMMANDS = 240;	
	final private int m_cmdBuffer[] = new int[MAX_COMMANDS];
	
	public TickCommandBuffer()
	{
		reset();
	}
	
	public void reset()
	{
		for (int i = 0; i < MAX_COMMANDS; i++)
		{
			m_cmdBuffer[i] = 1000;
		}
	}
	
	public void setCommand(int tick, int command)
	{
		m_cmdBuffer[tick%MAX_COMMANDS] = command;
	}
	
	public int getCommand(int tick)
	{
		return m_cmdBuffer[tick%MAX_COMMANDS];
	}

	public void set(TickCommandBuffer src) {
		for (int i = 0; i < MAX_COMMANDS; i++)
		{
			m_cmdBuffer[i] = src.getCommand(i);
		}		
	}
	
}
