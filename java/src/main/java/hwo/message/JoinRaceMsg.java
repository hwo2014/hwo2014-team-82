package hwo.message;

public class JoinRaceMsg
{
	public String msgType = "joinRace";
	class Data
	{
		public BotId botId;
		public String trackName;
		public String password;
		public int carCount;
	}
	public Data data = new Data();

    public JoinRaceMsg(BotId bot, String trackName, int carCount, String password) 
    {
        data.botId = bot;
        data.trackName = trackName;
        data.password = password;
        data.carCount = carCount;
    }
}