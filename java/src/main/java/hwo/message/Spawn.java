package hwo.message;

public class Spawn 
{
	
	public String msgType;
	public Ident data;
	public String gameId; 
	public int gameTick;
	
	public Spawn(String msgType, Ident data, String gameId, int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}
