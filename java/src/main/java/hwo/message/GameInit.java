package hwo.message;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GameInit 
{		
	public class Data
	{	
		public class TrackData
		{		
			public class Piece
			{
				public double length;
				@SerializedName("switch")
				public boolean hasSwitch;
				public int radius;
				public double angle;
				
				Piece(double length, boolean hasSwitch, int radius, double angle)
				{
					this.length = length;
					this.hasSwitch = hasSwitch;
					this.radius = radius;
					this.angle = angle;
				}
			}
			
			public class LaneData
			{
				public int distanceFromCenter;
				public int index;
				LaneData(int distanceFromCenter, int index)
				{
					this.distanceFromCenter = distanceFromCenter;
					this.index = index;
				}
			}
			
			public class StartingPoint
			{
				public Position position;
				public double angle;
				StartingPoint(Position position, double angle)
				{
					this.position = position;
					this.angle = angle;
				}
				
			}
			
			class Position
			{
				double x;
				double y;
				
				Position(double x, double y)
				{
					this.x = x;
					this.y = y;
				}
			}
			
			public String id;
			public String name;
			public List<Piece> pieces;
			public List<LaneData> lanes;
			public StartingPoint startingPoint;
			
			TrackData(String id, String name, List<Piece> pieces, List<LaneData> lanes, StartingPoint startingPoint)
			{
				this.id = id;
				this.name = name;
				this.pieces = pieces;
				this.lanes = lanes;
				this.startingPoint = startingPoint;
			}
			
		}
		
		public class CarData
		{
				
			class Dimensions
			{
				double length;
				double width;
				double guideFlagPosition;
				Dimensions(double length, double width, double guideFlagPosition)				
				{
					this.length = length;
					this.width = width;
					this.guideFlagPosition = guideFlagPosition;
				}
				
				
			}
			public Ident id;
			Dimensions dimensions;	
			
			CarData(Ident id, Dimensions dimensions)
			{
				this.id = id;
				this.dimensions = dimensions;
			}
			
		}
		
		public class RaceSession
		{
			int laps;
			int maxLapTimeMs;			
			boolean quickRace;
			double durationMs;
			RaceSession(int laps, int maxLapTimeMs, boolean quickRace)
			{
				this.laps = laps;
				this.maxLapTimeMs = maxLapTimeMs;
				this.quickRace = quickRace;
			}
		}
		
		public class Race
		{		
			public TrackData track;
			public List<CarData> cars;
			public RaceSession raceSession;
			Race(TrackData track, List<CarData> cars, RaceSession raceSession)
			{
				this.track = track;
				this.cars = cars;
				this.raceSession = raceSession;
			}
			
		}
		
		public Race race;
		
		Data(Race race)
		{
			this.race = race;
		}
	}

	
	
	public final String msgType;
	public Data data;
 
	
	GameInit(String msgType, Data data)
	{
		this.msgType = msgType;
		this.data = data;
	}
	
}
