package hwo.message;


public class SwitchMsg extends SendMsg 
{
    private boolean isLeft;

    public SwitchMsg(boolean isLeft) 
    {
        this.isLeft = isLeft;
    }

    @Override
    protected Object msgData() 
    {
    	if (isLeft)
    	{
    		return "Left";
    	}
    	else
    	{
    		return "Right";
    	}
    }

    @Override
    protected String msgType() 
    {
    	return "switchLane";
    }
}

