package hwo.message;


public class PingMsg extends SendMsg 
{
    @Override
    protected String msgType() 
    {
        return "ping";
    }
}