package hwo.message;

public class TurboAvailable 
{	
	public class TurboAvailableData
	{
		public double turboDurationMilliseconds;
		public int turboDurationTicks;
		public double turboFactor;
		public TurboAvailableData(double turboDurationMilliseconds, 
								  int turboDurationTicks, 
								  double turboFactor)
		{
			this.turboDurationMilliseconds = turboDurationMilliseconds;
			this.turboDurationTicks = turboDurationTicks;
			this.turboFactor = turboFactor;
		}
	}
	
	public String msgType;		
	public TurboAvailableData data;
	
	public TurboAvailable(String msgType, TurboAvailableData data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}
