package hwo.message;

public class YourCar 
{	
	public String msgType;
	public Ident data;
	public String gameId; 
	
	public YourCar(String msgType, Ident data, String gameId, int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
	}
}
