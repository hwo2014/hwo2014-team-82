package hwo.message;

import java.util.List;

public class CarPositions 
{
	public class CarPositionData
	{

		public class PiecePosition
		{
			public class Lane
			{
				public int startLaneIndex;
				public int endLaneIndex;  
				
				Lane(int start, int end)
				{
					this.startLaneIndex = start;
					this.endLaneIndex = end;
				}
			}
			
			public int pieceIndex;
			public double inPieceDistance;
			public Lane lane;
			public int lap;

			
			PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap)
			{
				this.pieceIndex = pieceIndex;
				this.inPieceDistance = inPieceDistance;
				this.lane = lane;
				this.lap = lap;
			}			
		}

		public Ident 		id;
		public double 	angle;
		public PiecePosition piecePosition;
		
		CarPositionData(Ident id, double angle, PiecePosition piecePosition)
		{
			this.id = id;
			this.angle = angle;
			this.piecePosition = piecePosition;
		}
		
	}
	
	public final String msgType;
	public List<CarPositionData> data;
	public String gameId;
	public int gameTick;
	    	
	
	CarPositions(String msgType, List<CarPositionData> data, String gameId, int gameTick) 
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
	
}
