package hwo.message;

import hwo.base.Version;
import hwo.message.GameInit.Data.CarData;
import hwo.race.LocalCar;
import hwo.race.Car;
import hwo.race.Race;
import hwo.race.Track;






import hwo.race.TurboInfo;

import com.google.gson.Gson;

public class MsgProcessor 
{
	final Gson gson = new Gson();

	public final static int RESULT_OK = 0;
    public final static int RESULT_PING = 1;
    public final static int RESULT_IGNORE = 2;
    public final static int RESULT_EXIT = 3;
	
    private Race m_race;
    private Ident m_myIdent;
    
    public MsgProcessor(Race race)
    {
    	m_race = race;
    }   
 
    
	public int handle(String line)
	{
       	final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
		int result = RESULT_IGNORE;
        if (msgFromServer.msgType.equals("carPositions")) 
        {        	
          	final CarPositions carPos = gson.fromJson(line, CarPositions.class);
          	for (int i = 0; i < carPos.data.size(); i++)
          	{
          		Car car = m_race.findCar(carPos.data.get(i).id);
          		if (car != null)
          		{
          			car.setPosition(carPos.gameTick, carPos.data.get(i));
          		}
          	}
          	m_race.setTick(carPos.gameTick);
        	result = RESULT_OK;        	
        	if (Version.DEBUG_SERVER)
        	{
        		if (m_myIdent != null)
        		{
        			Car car = m_race.findCar(m_myIdent);
        			if (car != null)
        			{
        				System.out.println("["+m_race.getTick()+"]:"+car.getThrottle()+" speed:"+car.getSpeed());
        			}
        		}
        	}        	
        } 
        else if (msgFromServer.msgType.equals("join")) 
        {
        	if (Version.DEBUG_SERVER)
        	{
        		System.out.println("Joined");
        	}
        	m_race.setTick(-1);
        } 
        else if (msgFromServer.msgType.equals("gameInit")) 
        {
        	final GameInit gameInit = gson.fromJson(line, GameInit.class);
        	Track track = new Track(gameInit.data.race.track);   
        	final int numCars = gameInit.data.race.cars.size();
        	m_race.setTrack(track);
        	m_race.setSession(gameInit.data.race.raceSession);
        	m_race.initCars(numCars);
        	
        	for (int i = 0; i < numCars; i++)
        	{
        		CarData data = gameInit.data.race.cars.get(i);
        		Car car;
        		if (m_myIdent != null && m_myIdent.hasMatchingId(data.id))
        		{
        			car = new LocalCar(data);
        			m_race.setCar(i, car);
        			m_race.setMyCar(car);
        		}
        		else
        		{
        			car = new Car(data);
        			m_race.setCar(i, car);
        		}
        		car.setRace(m_race);
        	}
        
        } 
        else if (msgFromServer.msgType.equals("gameEnd")) 
        {
        	if (Version.DEBUG_SERVER)
        	{
        		System.out.println("Race end");
        	}
        	m_race.stop();
        } 
        else if (msgFromServer.msgType.equals("gameStart")) 
        {
        	if (Version.DEBUG_SERVER)
        	{
        		System.out.println("Race start");
        	}
        	m_race.start();
        } 
        else if (msgFromServer.msgType.equals("crash"))
        {
        	final Crash crash = gson.fromJson(line, Crash.class);
        	Car car = m_race.findCar(crash.data);
        	if (car != null)
        	{
        		car.stop();
        	}
        	m_race.setTick(crash.gameTick);
        }
        else if (msgFromServer.msgType.equals("spawn"))
        {        	
        	final Spawn spawn = gson.fromJson(line, Spawn.class);
        	Car car = m_race.findCar(spawn.data);
        	if (car != null)
        	{
          		car.start();
          	}        		
          	m_race.setTick(spawn.gameTick);
        }
        else if (msgFromServer.msgType.equals("finish"))
        { 
        	final Finish finish = gson.fromJson(line, Finish.class);
        	Car car = m_race.findCar(finish.data);
         	if (car != null)
          	{
          		car.stop();
          	}  
          	m_race.setTick(finish.gameTick);
        }
        else if (msgFromServer.msgType.equals("yourCar"))
        { 
        	final YourCar yourCar = gson.fromJson(line, YourCar.class);
        	m_myIdent = yourCar.data;
        }
        else if (msgFromServer.msgType.equals("turboAvailable"))
        { 
        	final TurboAvailable turboAvailable = gson.fromJson(line, TurboAvailable.class);
        	m_race.setTurboInfo(new TurboInfo(
        			turboAvailable.data.turboDurationTicks, 
        			turboAvailable.data.turboFactor));
        }         
        else if (msgFromServer.msgType.equals("joinRace"))
        { 
        	
        }
        else if (msgFromServer.msgType.equals("lapFinished"))
        { 
        	
        }        	
        else
        {
       		System.err.println(msgFromServer.msgType);        
        }
        return result;
	}

	
}
