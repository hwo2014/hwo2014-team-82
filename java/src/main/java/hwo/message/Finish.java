package hwo.message;

public class Finish 
{
	
	public String msgType;
	public Ident data;
	public String gameId; 
	public int gameTick;
	
	public Finish(String msgType, Ident data, String gameId, int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
	}
}
