package hwo.message;

public class Ident {

	public String name;
	public String color;    	
		
	Ident(String name, String color)
	{
		this.name = name;
		this.color = color;	
	}

	public boolean hasMatchingId(Ident id) 
	{
		return this.color.equals(id.color) && this.name.equals(id.name);
	}
}
