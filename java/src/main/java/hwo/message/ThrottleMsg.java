package hwo.message;

public class ThrottleMsg
{
	final public String msgType = "throttle";
	public double data;
	public int tick;
	
	public ThrottleMsg(double data, int tick)
	{
		this.data = data;
		this.tick = tick;
	}
}