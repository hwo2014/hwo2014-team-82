package hwo.debug;

import hwo.ai.ConfigFinder;
import hwo.base.Bot;
import hwo.base.Client;
import hwo.message.MsgProcessor;

public class AnalysisClient extends Client 
{
    public AnalysisClient()
    {   
    	super();
    }
    
    @Override
    public void run()
    {
    	TestVideo3 video = new TestVideo3();
    	int step = 0;
    	int currentTick = -1;
    	while(m_isActive && video.output[step] != null)
    	{
    		int result = m_msgProcessor.handle(video.output[step]);
        	if (result == MsgProcessor.RESULT_OK)
        	{
        		int input = video.input[step];
        		if (input >=0 )
        		{
        			m_race.setThrottle(input);        			
        		}
        		else if (input == Bot.CMD_PING)
        		{
        			// PING
        		}
        		else if (input == Bot.CMD_SWITCH_RIGHT)
        		{
        			m_race.switchRight();
        		}
        		else if (input == Bot.CMD_SWITCH_LEFT)
        		{
        			m_race.switchLeft();
        		}
        		else
        		{
        			System.out.println("Invalid input");
        			break;
        		}
        	}
        	if (m_race.getTick() != currentTick)
        	{
        		currentTick = m_race.getTick();
        		m_race.update();
        	}
    		step++;
    		/*if (step == 10)
    		{
    			TickCommandBuffer tmp = new TickCommandBuffer();
    			TimeMachine m = new TimeMachine(tmp, m_race);
    			m.init();
    			m.begin();
    			m.end();
    			m.deinit();
    			break;
    		}*/
    		if (step == 1300)
    		{
    			ConfigFinder c = new ConfigFinder(m_race);
    			c.begin();
    			try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			c.end();
    			c.waitForDone();
    			break;
    		}
    	}
    }
	
}
