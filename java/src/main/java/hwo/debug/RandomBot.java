package hwo.debug;

import hwo.base.Bot;
import hwo.race.Race;

public class RandomBot extends Bot {

	public RandomBot(Race race) 
	{
		super(race);
	}

	public int tick()
	{
		if (Math.random() < 0.1)
		{
			return 1000;			
		}
		else if (Math.random() < 0.75)
		{
			return CMD_PING;
		}
		else if (Math.random() < 0.9)
		{        			
			return (int)(Math.random() * 600)+400;
		}
		else if (Math.random() < 0.5)
       	{
			return CMD_SWITCH_LEFT;
       	}
       	else
       	{
       		return CMD_SWITCH_RIGHT;
       	}
	}
	
}
