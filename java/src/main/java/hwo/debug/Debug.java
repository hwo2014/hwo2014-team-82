package hwo.debug;

public class Debug {

	
	public static final boolean ENABLED = true;

	public static void error(boolean state, String msg)
	{
		if (ENABLED)
		{
			if (!state)
			{
				System.err.println(msg);
			}
		}
	}
	
}
