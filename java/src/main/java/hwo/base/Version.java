package hwo.base;

public class Version {

	public static final int MODE_NORMAL = 0;
	public static final int MODE_LOCAL_TEST = 1;
	public static final int MODE_SAMPLER = 2;
	
	// 
	// Build setup
	// 
	
	public static final int 		MODE = MODE_SAMPLER;	
	public static final boolean 	DEBUG_SERVER = true;
	public static final boolean     AUTO_RECONNECT = false;
		
}
