package hwo.base;

import hwo.message.BotId;
import hwo.message.JoinRaceMsg;
import hwo.message.PingMsg;
import hwo.message.SendMsg;
import hwo.message.SwitchMsg;
import hwo.message.ThrottleMsg;
import hwo.message.TurboMsg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public abstract class NetClient extends Client 
{
    protected PrintWriter m_writer;
    
    private String m_host;
    private int m_port;

    public NetClient(String host, int port)  
    {    	
    	super();
    	m_host = host;
    	m_port = port;
    }
    
    @Override
    public void run()
    {
    	do
    	{    		
    		Socket socket = null;
    		try
    		{
    			System.out.println("Connecting to " + m_host + ":" + m_port);
    			socket = new Socket(m_host, m_port);
    			System.out.println("Socket ready");

    			m_writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));		
    			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));	     	
    			run(reader);    		
    		} 
    		catch (IOException e) 
    		{
    			e.printStackTrace();
    		}
    		
    		try    	
    		{
    			if (socket != null)
    			{
    				socket.close();
    			}
    		}
    		catch (IOException e) 
    		{
    			e.printStackTrace();
    		}
    	} while (checkReconnecting());
    }
    
    private boolean checkReconnecting()
    {
    	try 
    	{
    		if (Version.AUTO_RECONNECT)
    		{
    			wait(1000);
    		}
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
    	return Version.AUTO_RECONNECT;
    }
    
    abstract protected void run(BufferedReader reader) throws IOException;   
    
    protected void send(final SendMsg msg) 
    {
    	m_writer.println(msg.toJson());
    	m_writer.flush();
    }

    protected void sendJoinRace(String botName, String botKey, 
    		                    String track, int numCars, String password)
    {
    	JoinRaceMsg data = new JoinRaceMsg(new BotId(botName, botKey), track, numCars, password);
    	m_writer.println( new Gson().toJson(data) );   			
    	m_writer.flush();
    }
    
    protected void sendThrottle(int value)
    {
    	ThrottleMsg data = new ThrottleMsg((double)(value)/1000.0, m_race.getTick()+1);
    	m_writer.println( new Gson().toJson(data) );   			
    	m_writer.flush();
    }
    
    protected void sendSwitchLeft()
    {
		send(new SwitchMsg(true));
    }

    protected void sendSwitchRight()
    {
		send(new SwitchMsg(false));
    }
    
    protected void sendTurbo()
    {
    	send(new TurboMsg());
    }
    
    protected void sendPing()
    {
		send(new PingMsg());
    }
    

}
