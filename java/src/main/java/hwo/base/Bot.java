package hwo.base;

import hwo.race.Race;

public abstract class Bot 
{
	public final static int CMD_PING = -1;
	public final static int CMD_SWITCH_RIGHT = -2;
	public final static int CMD_SWITCH_LEFT = -3;
	public final static int CMD_TURBO = -4;
	
	protected Race m_race;
	
	public Bot(Race race)
	{
		m_race = race;
	}
	
	abstract public int tick();
	
	public void start()
	{
	}
	
	public void stop()
	{
	}
}

