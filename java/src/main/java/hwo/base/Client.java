package hwo.base;

import hwo.message.MsgProcessor;
import hwo.race.Race;

public class Client extends Thread 
{
	protected boolean m_isActive = true;
    final protected MsgProcessor m_msgProcessor;   
    protected Race m_race = new Race();
	
    public Client()
    {
    	m_msgProcessor = new MsgProcessor(m_race);
    }

	public void finish() 
	{
		m_isActive = false;		
	}
}
