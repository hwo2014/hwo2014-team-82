package hwo.base;

import hwo.debug.AnalysisClient;
import hwo.race.RaceClient;

import java.io.IOException;

public class Main
{
	static Client thread[] = null;
	
	//
	// Arguments
	// [Host] [Port] [Bot Name] [Bot Key] [Track Name] [Number of Bots] 
	// [Number of free slots] [Password]
	// 
	// E.g. 
	// senna.helloworldopen.com 8091 semi zgU3ZxPG5SyrQA keimola
	// senna.helloworldopen.com 8091 semi zgU3ZxPG5SyrQA keimola 2 0     
	
	@SuppressWarnings("unused")
	public static void main(String... args) throws IOException 
	{		
		if (Version.MODE == Version.MODE_LOCAL_TEST)
		{
			thread = new Client[1];
			thread[0] = new AnalysisClient();
		}
		else if (args.length >= 2)
		{
			String host = args[0];
			int port = Integer.parseInt(args[1]);

			String botName = "dummy";
			if (args.length >= 3)
			{
				botName = args[2];
			}
			
			String botKey = "";
			if (args.length >= 4)
			{
				botKey = args[3];
			}

			String track = null;
			if (args.length >= 5)
			{
				track = args[4];
			}			
			
			int numBots = 1;
			if (args.length >= 6)
			{
				numBots = Integer.parseInt(args[5]);
			}

			int numFree = 0;
			if (args.length >= 7)
			{
				numFree = Integer.parseInt(args[6]);
			}
			
			String password = "";
			if (args.length >= 8)
			{
				password = args[7];
			}

			thread = new Client[numBots];
			thread[0] = new RaceClient(0, host, port, botName, botKey, track, numBots + numFree, password);			
			for (int i = 1; i < numBots; i++)
			{
				thread[i] = new RaceClient(i, host, port, botName+""+(i+1), botKey, track, numBots + numFree, password);
			}
		}
		else
		{
			System.err.println("Invalid args");
		}

		if (thread != null)
		{
			Runtime.getRuntime().addShutdownHook(new Thread() {

			    @Override
			    public void run() 
			    {
					for (int i = 0; i < thread.length; i++)
					{
						thread[i].finish();
					}
			    }

			});
			
			try 
			{
				for (int i = 0; i < thread.length; i++)
				{
					thread[i].start();
					if (i == 0)
					{
						// TODO: Add proper waiting
						// Allow host to run first
						Thread.sleep(1000);
					}
				}
				for (int i = 0; i < thread.length; i++)
				{
					thread[i].join();
				}
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}		
		System.out.println("OK.");
	}  
}
